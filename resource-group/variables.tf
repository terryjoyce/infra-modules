variable "prefix" {}
variable "environment-short" {}
variable "location" {}
variable "location-short" {}
variable "suffix" {}
variable "tags" {  
	type        = map(any)
    default     = {}
  }
