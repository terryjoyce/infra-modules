resource "azurerm_resource_group" "main" {
  name     = "${var.prefix}-${var.environment-short}-${var.location-short}-${var.suffix}"
  location = var.location
  tags = var.tags
}
