variable "resource-group-name" {}
variable "environment-short" {}
variable "location" {}
variable "location-short" {}
variable "app-service-plan-id" {}
variable "prefix" {}
variable "suffix" {}
variable "https-only" {}
variable "client-affinity" {}
variable "always-on" {}
variable "min-tls-version" {}
variable "ftps-state" {}
variable "http2-enabled" {}
variable "site-config" {
  type        = map(any)
  default     = {}
  }
variable "app-settings" {
  type        = map(any)
  default     = {}
  }
variable "tags" {  
type        = map(any)
  default     = {}
}
variable "allowed-ips" {  
type        = map(any)
  default     = {}
}
