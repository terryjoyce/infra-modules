resource "azurerm_app_service_plan" "main" {
name                  = "${var.prefix}-${var.environment-short}-${var.location-short}-${var.suffix}"
  location            = var.location
  resource_group_name = var.resource-group-name
  kind                = var.type
  reserved            = true

  sku {
	  tier = var.tier
	  size = var.size
  }
  
  tags = var.tags
}



