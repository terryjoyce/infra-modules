variable "tier" {}
variable "type" {}
variable "size" {}
variable "location" {}
variable "location-short" {}
variable "environment-short" {}
variable "resource-group-name" {}
variable "prefix" {}
variable "suffix" {}
variable "tags" {  
type        = map(any)
default     = {}
}