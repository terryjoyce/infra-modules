resource "azurerm_frontdoor" "frontdoor" {
name                                         = "${var.prefix}-${var.environment-short}-${var.suffix}"
resource_group_name                          = var.resource-group-name
enforce_backend_pools_certificate_name_check = false

routing_rule {
  name               = "DefaultBackendForwardRule"
  accepted_protocols = ["Https"]
  patterns_to_match  = ["/*"]
  frontend_endpoints = ["FrontendEndpoint"]
  forwarding_configuration {
    forwarding_protocol = "HttpsOnly"
    backend_pool_name   = "BackendWebapps"
  }
}

# Routing rule to redirect all HTTP traffic to HTTPS endpoint
routing_rule {
  name               = "HTTPSRedirect"
  accepted_protocols = ["Http"]
  patterns_to_match  = ["/*"]
  frontend_endpoints = ["FrontendEndpoint"]
  redirect_configuration {
    redirect_protocol = "HttpsOnly"
    redirect_type     = "Moved"
  }
}

backend_pool_load_balancing {
  name                            = "LoadBalancingSettings"
  additional_latency_milliseconds = 200 # adding some extra latency ensures even load balancing between both regions, no matter where the client sits
}

backend_pool_health_probe {
  name                = "HealthProbeSetting"
  protocol            = "Https"
  probe_method        = "HEAD"
  path                = "/api/Healthz"
  interval_in_seconds = 30
}

backend_pool {
name = "BackendWebapps"

dynamic "backend" {
  for_each = var.backend-locations
  content {
    host_header = "${backend.value}"
    address     = "${backend.value}"
    http_port   = 80
    https_port  = 443
    enabled     = true
  }
}

load_balancing_name = "LoadBalancingSettings"
health_probe_name   = "HealthProbeSetting"
}

backend_pools_send_receive_timeout_seconds = var.backend-timeout


frontend_endpoint {
  name                              = "FrontendEndpoint"
  host_name                         = "${var.prefix}-${var.environment-short}-${var.suffix}.azurefd.net"
  custom_https_provisioning_enabled = false
  session_affinity_enabled          = false
}

tags = var.tags
}