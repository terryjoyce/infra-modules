variable "resource-group-name" {}
variable "prefix" {}
variable "environment-short" {}
variable "backend-timeout" {}
variable "suffix" {}
variable "backend-locations" {
type     = map(any)
default  = {}
}
variable "tags" {  
  type     = map(any)
  default  = {}
}
