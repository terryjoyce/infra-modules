output "azurerm-app-service-id" {
  value = azurerm_app_service.main.id
}

output "azurerm-app-service-default-site-hostname" {
  value = azurerm_app_service.main.default_site_hostname
}

output "azurerm-app-service-possible-outbound-ip-addresses" {
  value = azurerm_app_service.main.possible_outbound_ip_addresses
}
