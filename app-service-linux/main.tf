resource "azurerm_app_service" "main" {
  name                    = "${var.prefix}-${var.environment-short}-${var.location-short}-${var.suffix}"
  location 			          = var.location
  resource_group_name     = var.resource-group-name
  app_service_plan_id     = var.app-service-plan-id
  https_only              = var.https-only
  client_affinity_enabled = var.client-affinity

  site_config {
    always_on       = var.always-on
    min_tls_version = var.min-tls-version
    ftps_state      = var.ftps-state
    http2_enabled   = var.http2-enabled
    linux_fx_version = "DOCKER|${var.docker-registry-url}/111-${var.type}:INVALID"

 # IP restrictions based on IP ranges
   dynamic "ip_restriction" {
     for_each = var.allowed-ips
     content {
       name       = ip_restriction.key
       ip_address = ip_restriction.value
      }
    }

    scm_ip_restriction {
      name       = "Deny all"
      ip_address = "0.0.0.0/0"
      action     = "Deny"
    }
  }

  app_settings = var.app-settings

  tags = var.tags

  lifecycle {
    ignore_changes = [
      site_config.0.scm_type,
      site_config.0.linux_fx_version
    ]
  } 
} 